gnome-contacts (48~beta-1) unstable; urgency=medium

  * New upstream release

 -- Jeremy Bícha <jbicha@ubuntu.com>  Wed, 12 Feb 2025 18:25:16 -0500

gnome-contacts (47.1.1-1) unstable; urgency=medium

  * New upstream release

 -- Jeremy Bícha <jbicha@ubuntu.com>  Mon, 16 Dec 2024 13:11:01 -0500

gnome-contacts (47.0-1) unstable; urgency=medium

  * New upstream release
  * Bump Standards Version to 4.7.0
  * Release to unstable

 -- Jeremy Bícha <jbicha@ubuntu.com>  Tue, 17 Sep 2024 17:22:39 -0400

gnome-contacts (47~alpha-1) experimental; urgency=medium

  * New upstream release
  * Bump minimum glib, GTK4, libadwaita, & libportal
  * Add Build-Depends: libgstreamer1.0-dev for restored camera feature
    that now uses the xdg camera portal
  * Remove remaining patch: applied in new release

 -- Jeremy Bícha <jbicha@ubuntu.com>  Mon, 12 Aug 2024 11:29:06 -0400

gnome-contacts (46.0-2) unstable; urgency=medium

  * Cherry-pick patch to fix crash when glib is built with glib_debug

 -- Jeremy Bícha <jbicha@ubuntu.com>  Sat, 27 Jul 2024 15:49:41 -0400

gnome-contacts (46.0-1) unstable; urgency=medium

  * New upstream release
  * Drop unused Build-Depends: gnome-desktop3
  * Update Homepage

 -- Jeremy Bícha <jbicha@ubuntu.com>  Thu, 28 Mar 2024 11:57:04 -0400

gnome-contacts (46~beta-1) unstable; urgency=medium

  * Team upload
  * New upstream release
    - 46~alpha:
      + When importing new contacts, the confirmation dialog will also
        helpfully preview the names of the contacts to import
      + Searching for a contact through GNOME Shell and clicking it
        while editing an existing one could lead to a broken state.
        Now, Contacts will first confirm whether it's okay to stop
        editing.
      + A bug where (un)selecting a contact would fail when clicking
        the checkbox directly is now fixed
      + The nightly Flatpak should now properly show avatars for
        contacts
      + Some outdated and missing info was added in the app
        description
    - 46~beta:
      + Phone numbers are shown from left to right, regardless of the
        configured text direction
  * Stop using debian/control.in
  * debian/control: Bump minimum meson to 0.59

 -- Amin Bandali <bandali@ubuntu.com>  Tue, 27 Feb 2024 14:56:21 -0500

gnome-contacts (45.0-1) unstable; urgency=medium

  * New upstream release

 -- Jeremy Bícha <jbicha@ubuntu.com>  Wed, 20 Sep 2023 14:45:13 -0400

gnome-contacts (45~beta-2) unstable; urgency=medium

  * Release to unstable

 -- Jeremy Bícha <jbicha@ubuntu.com>  Wed, 30 Aug 2023 18:46:45 -0400

gnome-contacts (45~beta-1) experimental; urgency=medium

  * New upstream release
  * debian/control.in: Bump minimum GTK4, libadwaita, & vala

 -- Jeremy Bícha <jbicha@ubuntu.com>  Wed, 16 Aug 2023 07:43:56 -0400

gnome-contacts (44.0-2) unstable; urgency=medium

  * Release to unstable

 -- Jeremy Bícha <jbicha@ubuntu.com>  Fri, 21 Jul 2023 13:01:18 -0400

gnome-contacts (44.0-1) experimental; urgency=medium

  * New upstream release

 -- Jeremy Bicha <jbicha@ubuntu.com>  Mon, 27 Mar 2023 12:05:01 -0400

gnome-contacts (44~rc-1) experimental; urgency=medium

  * New upstream release

 -- Jeremy Bicha <jbicha@ubuntu.com>  Tue, 07 Mar 2023 18:22:36 -0500

gnome-contacts (44~beta-1) experimental; urgency=medium

  * New upstream release
  * debian/control.in: Bump minimum glib
  * debian/control.in: Build-Depend on libqrencode-dev

 -- Jeremy Bicha <jbicha@ubuntu.com>  Tue, 14 Feb 2023 10:24:14 -0500

gnome-contacts (43.1-1) unstable; urgency=medium

  [ Jeremy Bicha ]
  * New upstream release

  [ Debian Janitor ]
  * Add debian/upstream/metadata
  * Update standards version to 4.6.2, no changes needed

 -- Jeremy Bicha <jbicha@ubuntu.com>  Thu, 09 Feb 2023 18:22:20 -0500

gnome-contacts (43.0-1) unstable; urgency=medium

  * New upstream release

 -- Jeremy Bicha <jbicha@ubuntu.com>  Sun, 25 Sep 2022 17:05:29 -0400

gnome-contacts (43~rc-1) unstable; urgency=medium

  * New upstream release
  * Build against libsoup3 libraries

 -- Jeremy Bicha <jbicha@ubuntu.com>  Mon, 05 Sep 2022 23:55:39 -0400

gnome-contacts (43~beta-1) unstable; urgency=medium

  * New upstream release
  * debian/control.in: Bump minimum libadwaita, libportal, vala
  * debian/rules: Drop obsolete configure option

 -- Jeremy Bicha <jbicha@ubuntu.com>  Mon, 08 Aug 2022 13:26:50 -0400

gnome-contacts (42.0-2) unstable; urgency=medium

  * Revert "Add patch to revert dependency on gnome-control-center 42"
  * Add Breaks: evolution-ews << 3.44.1-2~ (Closes: #1009660)
  * Release to unstable

 -- Jeremy Bicha <jbicha@ubuntu.com>  Fri, 29 Apr 2022 15:41:17 -0400

gnome-contacts (42.0-1) experimental; urgency=medium

  * New upstream release
  * Switch to GTK4 and libadwaita
  * debian/control.in: Drop unused Build-Depends: cheese, libedataserverui
  * debian/control.in: Build-Depend on libportal for camera access
  * Drop patch: applied in new release
  * Add patch to revert dependency on gnome-control-center 42
  * Release to experimental since there are some issues with the new version

 -- Jeremy Bicha <jbicha@ubuntu.com>  Wed, 23 Mar 2022 15:43:18 -0400

gnome-contacts (41.0-2) unstable; urgency=medium

  * Add patch to support GNOME 42 dark theme preference
  * debian/control.in: Bump minimum libhandy to 1.5.90 for that change

 -- Jeremy Bicha <jeremy.bicha@canonical.com>  Tue, 15 Feb 2022 10:10:56 -0500

gnome-contacts (41.0-1) unstable; urgency=medium

  * New upstream release
  * debian/rules: update build flags
  * Bump Standards-Version to 4.6.0

 -- Jeremy Bicha <jbicha@debian.org>  Sun, 03 Oct 2021 16:41:04 -0400

gnome-contacts (40.0-3) unstable; urgency=medium

  * Bump debhelper-compat to 13
  * debian/rules: minor cleanup
  * Release to unstable

 -- Jeremy Bicha <jbicha@debian.org>  Sun, 22 Aug 2021 08:00:01 -0400

gnome-contacts (40.0-2) experimental; urgency=medium

  * debian/control: Bump dependency on libhandy-1 1.1.0

 -- Marco Trevisan (Treviño) <marco@ubuntu.com>  Wed, 02 Jun 2021 22:35:28 +0200

gnome-contacts (40.0-1) experimental; urgency=medium

  * New upstream release
  * debian/docs: Remove AUTHORS

 -- Marco Trevisan (Treviño) <marco@ubuntu.com>  Tue, 01 Jun 2021 03:46:47 +0200

gnome-contacts (3.38.1-1) unstable; urgency=medium

  * New upstream release

 -- Laurent Bigonville <bigon@debian.org>  Sun, 15 Nov 2020 12:38:56 +0100

gnome-contacts (3.38-1) unstable; urgency=medium

  * Team upload
  * New upstream release
  * Update libhandy dependency
  * Release to unstable

 -- Simon McVittie <smcv@debian.org>  Sun, 27 Sep 2020 18:20:16 +0100

gnome-contacts (3.37.2-1) experimental; urgency=medium

  * Team upload
  * New upstream release
  * d/control: Increase build-dependencies as per meson.build
  * d/rules: cheese option is now a feature, not a boolean
  * d/copyright: Update

 -- Simon McVittie <smcv@debian.org>  Tue, 01 Sep 2020 11:18:02 +0100

gnome-contacts (3.36.2-1) unstable; urgency=medium

  * Team upload
  * New upstream release
  * Canonicalize order of dependency lists (wrap-and-sort -a)

 -- Simon McVittie <smcv@debian.org>  Tue, 01 Sep 2020 10:00:20 +0100

gnome-contacts (3.36.1-1) unstable; urgency=medium

  * New upstream release

 -- Laurent Bigonville <bigon@debian.org>  Sat, 18 Apr 2020 12:53:16 +0200

gnome-contacts (3.36-1) unstable; urgency=medium

  * New upstream release

 -- Laurent Bigonville <bigon@debian.org>  Tue, 17 Mar 2020 11:11:51 +0100

gnome-contacts (3.35.90-1) experimental; urgency=medium

  * New upstream release
    - debian/control.in: Bump the build-dependencies
    - Drop d/p/Fix-show-contact-action-not-launching-app-if-closed.patch,
      applied upstream
  * debian/control.in: Bump Standards-Version to 4.5.0 (no further changes)

 -- Laurent Bigonville <bigon@debian.org>  Sun, 01 Mar 2020 16:57:10 +0100

gnome-contacts (3.34-2) unstable; urgency=medium

  * Team upload
  * Upload to unstable for the evolution-data-server 3.34 transition
  * Set Standards-Version to 4.4.1 (no changes required)

 -- Simon McVittie <smcv@debian.org>  Tue, 01 Oct 2019 09:15:23 +0100

gnome-contacts (3.34-1) experimental; urgency=medium

  * Team upload
  * New upstream release
  * Set Rules-Requires-Root to no
  * Set Standards-Version to 4.4.0 (no changes required)
  * d/p/Fix-show-contact-action-not-launching-app-if-closed.patch:
    Add a post-release bug fix from upstream

 -- Simon McVittie <smcv@debian.org>  Sat, 14 Sep 2019 17:16:07 +0100

gnome-contacts (3.33.91-1) experimental; urgency=medium

  [ Guido Günther ]
  * Always build-dep on appstream-utils.
    It's used in the override_dh_installdocs target.

  [ Iain Lane ]
  * debian/watch: Find unstable versions
  * New upstream release 3.33.90
  * BD on gnome-desktop 3.33. So we build against the new SONAME in exp

 -- Iain Lane <laney@debian.org>  Tue, 20 Aug 2019 18:21:09 +0100

gnome-contacts (3.32-1) experimental; urgency=medium

  * New upstream release
  * Bump minimum libhandy-0.0-dev to 0.0.9

 -- Jeremy Bicha <jbicha@debian.org>  Sun, 17 Mar 2019 06:57:29 -0400

gnome-contacts (3.31.90-1) experimental; urgency=medium

  * New upstream development release
  * debian/copyright: Exclude the libhandy code copy
  * Build-Depend on desktop-file-utils, libhandy-0.0-dev, and libxml2-utils
  * Build-Depend on debhelper-compat 12 and drop debian/compat
  * Build-Depend on dh-sequence-gir and dh-sequence-gnome

 -- Jeremy Bicha <jbicha@debian.org>  Wed, 06 Feb 2019 07:38:07 -0500

gnome-contacts (3.30.2-1) unstable; urgency=medium

  * New upstream release
  * Restore -Wl,-O1 to our LDFLAGS
  * Bump Standards-Version to 4.3.0

 -- Jeremy Bicha <jbicha@debian.org>  Sun, 23 Dec 2018 09:21:54 -0500

gnome-contacts (3.30.1-1) unstable; urgency=medium

  * New upstream release

 -- Jeremy Bicha <jbicha@debian.org>  Tue, 25 Sep 2018 11:16:25 -0400

gnome-contacts (3.30-1) unstable; urgency=medium

  * New upstream release
  * Update meson configure options for new release
  * Bump Standards-Version to 4.2.1
  * Add X-Ubuntu-Use-Langpack to opt in to Ubuntu language pack handling
    (LP: #1779574)
  * Stop building telepathy support. It is no longer supported by GNOME Online
    Accounts & Debian GNOME no longer installs Empathy by default.
    (LP: #1754803)

 -- Jeremy Bicha <jbicha@debian.org>  Fri, 07 Sep 2018 12:33:13 -0400

gnome-contacts (3.28.2-1) unstable; urgency=medium

  * New upstream release

 -- Tim Lunn <tim@feathertop.org>  Tue, 08 May 2018 16:59:56 +1000

gnome-contacts (3.28.1-1) unstable; urgency=medium

  * New upstream version

 -- Tim Lunn <tim@feathertop.org>  Fri, 13 Apr 2018 20:39:18 +1000

gnome-contacts (3.28.0-1) unstable; urgency=medium

  * New upstream version

 -- Tim Lunn <tim@feathertop.org>  Sat, 17 Mar 2018 18:03:59 +1100

gnome-contacts (3.27.92-1) unstable; urgency=medium

  * New upstream release candidate
  * Release to unstable

 -- Tim Lunn <tim@feathertop.org>  Sat, 10 Mar 2018 08:55:26 +1100

gnome-contacts (3.27.90-1) experimental; urgency=medium

  * New upstream development release
    - Embedded map for contact addresses is no longer available
  * Build with meson
  * Depend on telepathy-mission-control-5 to fix warnings if it's not
    installed

 -- Jeremy Bicha <jbicha@debian.org>  Tue, 13 Feb 2018 10:30:07 -0500

gnome-contacts (3.26.1-1) unstable; urgency=medium

  * New upstream release
  * Update Vcs fields for migration to https://salsa.debian.org/
  * Bump debhelper compat to 11
  * Bump Standards-Version to 4.1.3

 -- Jeremy Bicha <jbicha@debian.org>  Wed, 24 Jan 2018 08:10:46 -0500

gnome-contacts (3.26-2) unstable; urgency=medium

  * Update Vcs fields for conversion to git
  * Add debian/gbp.conf
  * Bump Standards-Version to 4.1.2

 -- Jeremy Bicha <jbicha@debian.org>  Thu, 14 Dec 2017 17:24:03 -0500

gnome-contacts (3.26-1) unstable; urgency=medium

  * New upstream release
  * Use appstream-util appdata-to-news to install NEWS

 -- Jeremy Bicha <jbicha@debian.org>  Sun, 17 Sep 2017 21:52:03 -0400

gnome-contacts (3.25.4-1) unstable; urgency=medium

  * New upstream release.
  * debian/control.in:
    - Drop Build-Depends on intltool
    - Bump dependencies according to configure.ac: folks >= 0.11.4,
      glib >= 2.44.0, gtk+ >= 3.22.0, & telepathy-glib >= 0.22.0
  * Bump Standards-Version to 4.1.0

 -- Jeremy Bicha <jbicha@debian.org>  Wed, 30 Aug 2017 13:11:56 -0400

gnome-contacts (3.22.1-1) unstable; urgency=medium

  * New upstream release.
  * Use non-multiarch path (/usr/lib/gnome-contacts) for libexecdir.

 -- Michael Biebl <biebl@debian.org>  Wed, 21 Sep 2016 18:22:16 +0200

gnome-contacts (3.20.0-2) unstable; urgency=medium

  * Convert from cdbs to dh.
  * Bump dh compat to 10 (automatic dh-autoreconf)

 -- Andreas Henriksson <andreas@fatal.se>  Tue, 13 Sep 2016 19:16:11 +0200

gnome-contacts (3.20.0-1) unstable; urgency=medium

  * New upstream release.

 -- Michael Biebl <biebl@debian.org>  Thu, 19 May 2016 02:15:20 +0200

gnome-contacts (3.19.91-2) unstable; urgency=medium

  * Bump gtk+ build-dependency to >= 3.20.0 (Closes: #821857)
    - uses gtk_widget_set_focus_on_click API new in 3.20, see also:
      https://git.gnome.org/browse/gnome-contacts/commit/?id=fefba769

 -- Andreas Henriksson <andreas@fatal.se>  Wed, 20 Apr 2016 14:14:45 +0200

gnome-contacts (3.19.91-1) unstable; urgency=medium

  [ Laurent Bigonville ]
  * New upstream release.
  * debian/control.in: Bump Standards-Version to 3.9.8 (no further changes)
  * debian/control.in: Bump BD on glib to 2.37.6, as per configure.ac

 -- Andreas Henriksson <andreas@fatal.se>  Tue, 19 Apr 2016 10:53:51 +0200

gnome-contacts (3.18.1-1) unstable; urgency=medium

  [ Andreas Henriksson ]
  * Remove 'src/*vala.stamp' to force rebuild from vala src (Closes: #802520)

  [ Michael Biebl ]
  * New upstream release.
  * Drop Build-Depends on libnotify-dev, no longer required.

 -- Michael Biebl <biebl@debian.org>  Thu, 12 Nov 2015 15:57:47 +0100

gnome-contacts (3.18.0-1) unstable; urgency=medium

  * New upstream release.
  * Update gtk+ build-dependency to >= 3.16.0

 -- Andreas Henriksson <andreas@fatal.se>  Thu, 24 Sep 2015 12:06:25 +0200

gnome-contacts (3.16.2-1) unstable; urgency=medium

  * New upstream release.
  * Update Build-Depends as per configure.ac:
    - Add intltool (>= 0.40).
    - Bump libebook1.2-dev to (>= 3.13.90).
    - Bump libedataserver1.2-dev to (>= 3.13.90).
    - Add libedataserverui1.2-dev (>= 3.13.90).
    - Add libchamplain-0.12-dev.
    - Add libclutter-gtk-1.0-dev.
    - Add libgeocode-glib-dev (>= 3.15.3).
  * Add Homepage: URL.
  * Bump debhelper compatibility level to 9.
  * Update debian/copyright to the final format version 1.0.

 -- Michael Biebl <biebl@debian.org>  Mon, 15 Jun 2015 18:00:20 +0200

gnome-contacts (3.14.2-1) unstable; urgency=medium

  [ Pedro Beja ]
  * New upstream release.

 -- Michael Biebl <biebl@debian.org>  Thu, 28 May 2015 01:58:17 +0200

gnome-contacts (3.14.1-1) unstable; urgency=medium

  * New upstream release.
  * debian/control.in: Bump Standards-Version to 3.9.6 (no further changes)

 -- Laurent Bigonville <bigon@debian.org>  Sat, 18 Oct 2014 00:41:04 +0200

gnome-contacts (3.14.0-1) unstable; urgency=medium

  * New upstream release.

 -- Andreas Henriksson <andreas@fatal.se>  Mon, 22 Sep 2014 21:17:55 +0200

gnome-contacts (3.13.92-1) unstable; urgency=medium

  [ Andreas Henriksson ]
  * Bump Standards-Version to 3.9.5

  [ Sjoerd Simons ]
  * New upstream release

 -- Sjoerd Simons <sjoerd@debian.org>  Sat, 20 Sep 2014 10:37:33 +0200

gnome-contacts (3.13.91-1) experimental; urgency=medium

  * New upstream development release.
  * Bump valac build-dependency to >= 0.24.0 according to configure.ac

 -- Andreas Henriksson <andreas@fatal.se>  Sun, 07 Sep 2014 11:45:12 +0200

gnome-contacts (3.12.0-1) unstable; urgency=low

  [ Simon McVittie ]
  * Team upload
  * New upstream release
    - has undeclared dependency on Gtk 3.12, update build-dependency
  * Use dh-autoreconf
  * Don't force use of GNOME 3.10 gnome-desktop

 -- Andreas Henriksson <andreas@fatal.se>  Wed, 02 Apr 2014 21:41:19 +0200

gnome-contacts (3.10.1-2) experimental; urgency=low

  * Build against G3.10 gnome-destkop

 -- Sjoerd Simons <sjoerd@debian.org>  Thu, 07 Nov 2013 08:06:01 +0100

gnome-contacts (3.10.1-1) experimental; urgency=low

  * New upstream release
  * Bump b-d on valac, folks and gtk+3

 -- Sjoerd Simons <sjoerd@debian.org>  Fri, 01 Nov 2013 19:23:45 +0100

gnome-contacts (3.8.3-1) unstable; urgency=low

  [ Jeremy Bicha ]
  * debian/control.in:
    - Drop alternate build-depends on valac-0.18 since it's no longer
      in Debian

  [ Michael Biebl ]
  * New upstream release.
  * Loosen Build-Depends on libgnome-desktop-3-dev, we do not strictly require
    version (>= 3.6.0) which is not yet available in unstable.
  * Bump Standards-Version to 3.9.4. No further changes.
  * Upload to unstable.

 -- Michael Biebl <biebl@debian.org>  Thu, 19 Sep 2013 18:23:06 +0200

gnome-contacts (3.8.0-1) experimental; urgency=low

  * New upstream release.

 -- Andreas Henriksson <andreas@fatal.se>  Thu, 04 Apr 2013 14:59:29 +0200

gnome-contacts (3.7.91-1) experimental; urgency=low

  [ Thomas Bechtold ]
  * New upstream release

  [ Sjoerd Simons ]
  * New upstream release (3.7.90)
  * debian/control: bump build-depends

  [ Andreas Henriksson ]
  * New upstream release (3.7.91)
  * Bump build-dependency on libfolks*-dev to (>= 0.9)
    - gnome-contacts uses libgee-0.8 so we need a libfolks that do too.
    - libgtk-3-dev (>= 3.7.10)

 -- Sjoerd Simons <sjoerd@debian.org>  Sun, 24 Mar 2013 18:24:03 +0100

gnome-contacts (3.6.1-1) experimental; urgency=low

  [ Jeremy Bicha ]
  * debian/control.in:
    - Build-depend on valac (>= 0.14.0) (Closes: #675643)

  [ Sjoerd Simons ]
  * New upstream release

 -- Sjoerd Simons <sjoerd@debian.org>  Thu, 18 Oct 2012 23:35:54 +0200

gnome-contacts (3.4.1-2) unstable; urgency=low

  [ Jeremy Bicha ]
  * debian/control.in:
    - Build-depend on valac (>= 0.14.0) (Closes: #675643)

  [ Michael Biebl ]
  * debian/patches/01-icon-rename.patch
    - Handle renaming of the preferences-system-date-and-time-symbolic icon in
      gnome-icon-theme-symbolic. Patch cherry-picked from upstream Git.

 -- Michael Biebl <biebl@debian.org>  Sun, 16 Jun 2013 21:41:08 +0200

gnome-contacts (3.4.1-1) unstable; urgency=low

  * New upstream release.
  * Bump Build-Depends on libgtk-3-dev to (>= 3.4.0).

 -- Michael Biebl <biebl@debian.org>  Mon, 30 Apr 2012 16:59:24 +0200

gnome-contacts (3.4.0-2) unstable; urgency=low

  * Upload to unstable.

 -- Michael Biebl <biebl@debian.org>  Sun, 22 Apr 2012 18:36:09 +0200

gnome-contacts (3.4.0-1) experimental; urgency=low

  [ Josselin Mouette ]
  * Update repository URL.

  [ Jeremy Bicha ]
  * New upstream release.
  * debian/control.in:
    - Build-depend on libfolks-eds-dev & libtelepathy-glib-dev
    - Bump glib to (>= 2.31.10)
    - Bump Standards-Version to 3.9.3

  [ Laurent Bigonville ]
  * debian/control.in:
    - Drop duplicate Section
    - Bump libgtk-3-dev build-dependency to 3.3.6, required by
      GtkApplicationWindow

 -- Laurent Bigonville <bigon@debian.org>  Tue, 03 Apr 2012 23:00:56 +0200

gnome-contacts (3.2.2-2) unstable; urgency=low

  * Upload to unstable.

 -- Michael Biebl <biebl@debian.org>  Sat, 17 Dec 2011 08:18:51 +0100

gnome-contacts (3.2.2-1) experimental; urgency=low

  [ Jeremy Bicha ]
  * Initial release. Closes: #646895

  [ Michael Biebl ]
  * Update to 3.2.2.
  * debian/watch: Track .xz tarballs.
  * debian/control.in:
    - Bump Build-Depends on libebook1.2-dev and libedataserver1.2-dev to
      (>= 3.2).

  [ Sjoerd Simons ]
  * Depend on valac-0.14 instead of valac (>= 0.14)

 -- Sjoerd Simons <sjoerd@debian.org>  Sun, 13 Nov 2011 14:27:36 +0100
